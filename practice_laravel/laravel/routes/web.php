<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('adminlte.index');
});
Route::get('/student/create', 'StudentController@create')->name('student.create')->middleware('login_auth');
Route::post('/student', 'StudentController@store')->name('student.store')->middleware('login_auth');
Route::get('/student', 'StudentController@index')->name('student.index')->middleware('login_auth');
Route::get('/student/{student}', 'StudentController@show')->name('student.show')->middleware('login_auth');
Route::get('/student/{student}/edit', 'StudentController@edit')->name('student.edit')->middleware('login_auth');
Route::patch('/student/{student}', 'StudentController@update')->name('student.update')->middleware('login_auth');
Route::delete('/student/{student}', 'StudentController@destroy')->name('student.destroy')->middleware('login_auth'); 

Route::get('/login', 'AdminController@index')->name('login.index');
Route::get('/logout', 'AdminController@logout')->name('login.logout');
Route::post('/login', 'AdminController@process')->name('login.process');


Route::get('/register/create', 'UserController@create')->name('register.create');
Route::get('/register', 'UserController@index')->name('register.index');
Route::post('/register', 'UserController@store')->name('register.store');


//-----------------------------------------
Route::get('/userlte/create', 'MahasiswaController@create')->name('userlte.create')->middleware('login_auth');  
Route::post('/userlte', 'MahasiswaController@store')->name('userlte.store')->middleware('login_auth');
Route::get('/userlte/{user}', 'MahasiswaController@index')->name('userlte.index')->middleware('login_auth');
Route::get('/userlte/{user}', 'MahasiswaController@show')->name('userlte.show')->middleware('login_auth');
Route::get('/userlte/{user}/edit', 'MahasiswaController@edit')->name('userlte.edit')->middleware('login_auth');
Route::patch('/userlte/{user}', 'MahasiswaController@update')->name('userlte.update')->middleware('login_auth');
Route::delete('/userlte/{user}', 'MahasiswaController@destroy')->name('userlte.destroy')->middleware('login_auth'); 
//--------------


Route::get('/adminlte/index', 'AdminLTEController@index')->name('adminlte.index');
Route::get('/adminlte/student/create', 'AdminLTEStudentController@create')->name('adminlte.student.create')->middleware('login_auth');
Route::get('/adminlte/student/createjob', 'JobController@createjob')->name('adminlte.student.createjob')->middleware('login_auth');
Route::post('/adminlte/student', 'AdminLTEStudentController@store')->name('adminlte.student.store');
Route::post('/adminlte/student/createjob', 'JobController@storejob')->name('adminlte.student.storejob');
Route::get('/adminlte/student', 'AdminLTEStudentController@index')->name('adminlte.student.index')->middleware('login_auth');

Route::get('/adminlte/student/indexjob', 'JobController@indexjob')->name('adminlte.student.indexjob')->middleware('login_auth');
Route::get('/adminlte', 'AdminLTEStudentController@dashboard')->name('adminlte.student.dashboard')->middleware('login_auth');
Route::get('/adminlte/student/{student}', 'AdminLTEStudentController@show')->name('adminlte.student.show')->middleware('login_auth');
Route::get('/adminlte/student/{student}/edit', 'AdminLTEStudentController@edit')->name('adminlte.student.edit');
Route::patch('/adminlte/student/{student}', 'AdminLTEStudentController@update')->name('adminlte.student.update');
Route::delete('/adminlte/student/{student}', 'AdminLTEStudentController@destroy')->name('adminlte.student.destroy');

Route::get('/adminlte/student/{job}', 'AdminLTEStudentController@showjob')->name('adminlte.student.showjob');
Route::get('/adminlte/student/{job}/edit', 'AdminLTEStudentController@edit')->name('adminlte.job.edit');
Route::patch('/adminlte/student/{job}', 'AdminLTEStudentController@update')->name('adminlte.job.update');
Route::delete('/adminlte/student/{job}', 'AdminLTEStudentController@destroy')->name('adminlte.job.destroy');
