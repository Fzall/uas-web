<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--===============================================================================================
	<link rel="icon" type="image/png" href="../login/images/icons/favicon.ico"/>-->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../login/css/util.css">
	<link rel="stylesheet" type="text/css" href="../login/css/main.css">
<!--===============================================================================================-->
    <title>Login</title>
</head>

<body>
<div class="limiter">
		<div class="container-login100" style="background-image: url('../login/images/bg-01.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					Account Login
				</span>
                
                @if(session()->has('pesan'))
                <div class="alert alert-success">
                    {{ session()->get('pesan') }}
                </div>
                @endif
                <form class="login100-form validate-form p-b-33 p-t-5" action="{{ route('login.process') }}" method="POST">
                    @csrf
                    <div class="form-group">
                    <div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="User name" class="form-control @error('username') is-invalid @enderror" id="username"
                             value="{{ old('username') }}">
                             @error('username')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
                       
                    </div>
                    <div class="form-group">
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password"  placeholder="Password"class="form-control @error('password') is-invalid @enderror"
                            id="password" name="password" value="{{ old('password') }}">
                            @error('password')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>
                    <div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Login
                        </button>
                    <div class="">
                        <br>
                        <br>
                        <br>
                        <h8>Create Your Account <a href="{{ url('') }}/login" class="submit"> Here !</h8></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</body>

</html>