@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar2')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="py-4 d-flex justify-content-end align-items-center">
                        <h2 class="mr-auto">Tabel Mahasiswa</h2>
                        <a href="{{ route('userlte.create') }}" class="btn btn-primary">
                            Tambah Mahasiswa
                        </a>
                    </div>
                    @if(session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Foto</th>
                                <th>Nim</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Jurusan</th>
                                <th>Alamat</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($students as $mahasiswa)
                            <tr>
                                <th>{{$loop->iteration}}</th>
                                <td><img height="30px" src="{{url('')}}/{{$mahasiswa->image}}" class="rounded" alt="">
                                </td>
                                <td><a
                                        href="{{ route('userlte.show',['user' => $mahasiswa->id])}}">{{$mahasiswa->nim}}</a>
                                </td>
                                <td>{{$mahasiswa->name}}</td>
                                <td>{{$mahasiswa->jenis_kelamin == 'P'?'Perempuan':'Laki-laki'}}</td>
                                <td>{{$mahasiswa->jurusan}}</td>
                                <td>{{$mahasiswa->alamat == '' ? 'N/A' : $mahasiswa->alamat}}</td>
                                <td>{{$mahasiswa->email}}</td>
                            </tr>
                            @empty
                            <td colspan=" 6" class="text-center">Tidak ada data...</td>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection