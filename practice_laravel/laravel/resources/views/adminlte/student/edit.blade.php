@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Mahasiswa
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container pt-4 bg-white">
            <div class="row">
                <div class="col-md-8 col-xl-6">

                    <hr>
                    <form action="{{ route('adminlte.student.update',['student' => $student->id]) }}" method="POST" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            <label for="nim">NIM</label>
                            <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim"
                                name="nim" value="{{ old('nim') ?? $student->nim }}">
                            @error('nim')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <br>
                            <div class="form-group">
                            <label for="nama">Nama Lengkap</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="nama"
                                name="nama" value="{{ old('name') ?? $student->name }}">
                            @error('name')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" id="nama"
                                name="email" value="{{ old('email') ?? $student->email }}">
                            @error('email')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                            <label for="username">Username</label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror" id="username"
                                name="username" value="{{ old('username') ?? $student->username }}">
                            @error('username')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <label for="password">Password</label>
                            <input type="text" class="form-control @error('password') is-invalid @enderror" id="password"
                                name="password" value="{{ old('password') ?? $student->password }}">
                            @error('password')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <!-- <div class="form-group">
                            <label for="nama">Nama Lengkap</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="nama"
                                name="nama" value="{{ old('name') ?? $student->name }}">
                            @error('name')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div> -->
                        <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                            <option value="Laki-Laki"
                                {{ old('jenis_kelamin')=='Laki-Laki L' ? 'selected': '' }}>
                                Laki-Laki
                            </option>
                            <option value="Perempuan"
                                {{ old('jenis_kelamin')=='Perempuan P' ? 'selected': '' }}>
                                Perempuan
                            </option>
                        </select>
                        @error('jenis_kelamin')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group">
                            <label for="jurusan">Jurusan</label>
                            <select class="form-control" name="jurusan" id="jurusan">
                                <option value="Teknik Informatika"
                                    {{ (old('jurusan') ?? $student->jurusan)=='Teknik Informatika' ? 'selected': '' }}>
                                    Teknik Informatika
                                </option>
                                <option value="Sistem Informasi"
                                    {{ (old('jurusan') ?? $student->jurusan)=='Sistem Informasi' ? 'selected': '' }}>
                                    Sistem Informasi
                                </option>
                                <option value="Ilmu Komputer"
                                    {{ (old('jurusan') ?? $student->jurusan)=='Ilmu Komputer' ? 'selected': '' }}>
                                    Ilmu Komputer
                                </option>
                                <option value="Teknik Komputer"
                                    {{ (old('jurusan') ?? $student->jurusan)=='Teknik Komputer' ? 'selected': '' }}>
                                    Teknik Komputer
                                </option>
                                <option value="Teknik Telekomunikasi"
                                    {{(old('jurusan')??$student->jurusan)=='Teknik Telekomunikasi'?'selected':''}}>
                                    Teknik Telekomunikasi
                                </option>
                            </select>
                            @error('jurusan')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" id="alamat" rows="3"
                                name="alamat">{{ old('alamat') ?? $student->alamat}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Gambar Profile</label>
                            <br><img height="150px" src="{{url('')}}/{{$student->image}}" class="rounded" alt="">
                            <input type="file" class="form-control-file" id="image" name="image">
                            @error('image')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection