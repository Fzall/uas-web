@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container pt-4 bg-white">
            <div class="row">
                <div class="col-md-8 col-xl-6">
                    <h1>Input Data Pekerjaan Orang Tua Mahasiswa</h1>
                    <hr>
                    <form action="{{ route('adminlte.student.storejob') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="nikayah">NIK</label>
                            <input type="text" class="form-control @error('nikayah') is-invalid @enderror" id="nikayah"
                                name="nikayah" value="{{ old('nikayah') }}">
                            @error('nikayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="namaayah">Nama Ayah</label>
                            <input type="text" class="form-control @error('namaayah') is-invalid @enderror" id="namaayah"
                                name="namaayah" value="{{ old('namaayah') }}">
                            @error('namaayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror</div>
                            <div class="form-group">
                            <label for="nikibu">NIK</label>
                            <input type="text" class="form-control @error('nikibu') is-invalid @enderror" id="nikibu"
                                name="nikibu" value="{{ old('nikibu') }}">
                            @error('nikibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                            <div class="form-group">
                            <label for="namaibu">Nama Ibu</label>
                            <input type="text" class="form-control @error('namaibu') is-invalid @enderror" id="namaibu"
                                name="namaibu" value="{{ old('namaibu') }}">
                            @error('namaibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror</div>

                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" id="alamat" rows="3"
                                name="alamat">{{ old('alamat') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="ayah">Pekerjaan Ayah</label>
                            <select class="form-control" name="ayah" id="ayah">
                                <option value="Pegawai Negeri"
                                    {{ old('ayah')=='Pegawai Negeri' ? 'selected': '' }}>
                                    Pegawai Negeri
                                </option>
                                <option value="Wiraswasta"
                                    {{ old('ayah')=='Salah Satu OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                    Wiraswasta
                            </select>
                            @error('ayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="ibu">Pekerjaan Ibu</label>
                            <select class="form-control" name="ibu" id="ibu">
                                <option value="Pegawai Negeri"
                                    {{ old('ibu')=='Pegawai Negeri' ? 'selected': '' }}>
                                    Pegawai Negeri
                                </option>
                                <option value="Wiraswasta"
                                    {{ old('ibu')=='Wira swasta' ? 'selected': '' }}>
                                    Wiraswasta
                                </option>
                                <option value="Ibu Rumah Tangga"
                                    {{ old('ibu')=='Ibu Rumah Tangga' ? 'selected': '' }}>
                                    ibu Rumah Tangga
                                </option>
                            </select>
                            @error('ibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="income_before">Pendapatan Sebelum Pandemi (Total)</label>
                            <input type="text" class="form-control @error('income_before') is-invalid @enderror" id="income_before"
                                name="income_before" value="{{ old('income_before') }}">
                            @error('income_before')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>



                        <div class="form-group">
                            <label for="income_after">Pendapatan Setelah Pandemi (Total)</label>
                            <input type="text" class="form-control @error('income_after') is-invalid @enderror" id="income_after"
                                name="income_after" value="{{ old('income_after') }}">
                            @error('income_after')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="faktor">Faktor Penghambat</label>
                            <select class="form-control" name="faktor" id="faktor">
                                <option value="OrangTua Di PHK"
                                    {{ old('faktor')=='OrangTua Di PHK' ? 'selected': '' }}>
                                    OrangTua Di PHK
                                </option>
                                <option value="Salah Satu OrangTua Meninggal Akibat Covid-19"
                                    {{ old('faktor')=='Salah Satu OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                    Salah Satu OrangTua Meninggal Akibat Covid-19
                                </option>
                                <option value="OrangTua Meninggal Akibat Covid-19" {{ old('faktor')=='OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                OrangTua Meninggal Akibat Covid-19
                                </option>
                            </select>
                            @error('faktor')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="image_income_before">Bukti Pendapatan Sebelum Pandemi</label>
                            <input type="file" class="form-control-file" id="image_income_before" name="image_income_before">
                            @error('image_income_before')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="image_income_after">Bukti Pendapatan Setelah Pandemi</label>
                            <input type="file" class="form-control-file" id="image_income_after" name="image_income_after">
                            @error('image_income_after')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary mb-2">Tambahkan</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection