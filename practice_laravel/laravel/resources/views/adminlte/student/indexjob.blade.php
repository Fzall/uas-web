@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>
    <!-- Main content -->
    <section class="content">
    <div class="container mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="py-4 d-flex justify-content-end align-items-center">
                        <h2 class="mr-auto">Data Orang Tua</h2>
                        <br>
                        <a href="{{ route('adminlte.student.createjob') }}" class="btn btn-primary">
                            Tambah Data Orang Tua
                        </a>
                    </div>
                    @if(session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                    @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>NIK</th>
                    <th>Nama Ayah</th>
                    <th>NIK</th>
                    <th>Nama Ibu</th>
                    <th>Alamat</th>
                    <th>Pekerjaan Ayah</th>
                    <th>Pekerjaan Ibu</th>
                    <th>Pendapatan Sebelum Pandemi</th>
                    <th>Pendapatan Sesudah Pandemi</th>
                    <th>Faktor Penghambat</th>
                    <th>Penurunan Pendapatan</th>
                </tr>
                @forelse ($students as $mahasiswa)
                <tr>
                <th>{{$loop->iteration}}</th>
                    <td><a href="">{{$mahasiswa->nikayah}}</a>
                    </td>
                    <td>{{$mahasiswa->namaayah}}</td>
                    <td>{{$mahasiswa->nikibu}}</td>
                    <td>{{$mahasiswa->namaibu}}</td>
                    <td>{{$mahasiswa->alamat}}</td>
                    <td>{{$mahasiswa->ayah}}</td>
                    <td>{{$mahasiswa->ibu}}</td>
                    <td>{{$mahasiswa->income_before}}</td>
                    <td>{{$mahasiswa->income_after}}</td>
                    <td>{{$mahasiswa->faktor}}</td>
                    <td>{{$mahasiswa->income_before-$mahasiswa->income_after}}</td>
                                
                </tr>
                    @empty
                    <td colspan=" 15" class="text-center">Tidak ada data...</td>
                    @endforelse
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection