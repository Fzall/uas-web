<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Beranda</title>

  <!-- Bootstrap Core CSS -->
  <link href="../templete/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="../templete/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="../templete/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="../templete/css/stylish-portfolio.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <a class="menu-toggle rounded" href="#">
    <i class="icon-arrow-up"></i>
  </a>

  <!-- Header -->
  <header class="masthead d-flex">
    <div class="container text-center my-auto">
      <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">WELCOME !</a>
    </div>
    <div class="overlay"></div>
  </header>

  <!-- About -->
  <section class="content-section bg-light" id="about">
    <div class="container text-center">
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h2>Pendataan Mahasiswa Institut Teknologi Telkom Purwokerto Dampak Covid-19 </h2>
          <p class="lead mb-5">Dimana Mahasiswa IT Telkom Purwokerto Dapat Mendaftarkan Diri Untuk Meringankan Beban Biaya Kuliah Karena Beberapa Faktor Akibat Pandemi !
            <a href="{{ route('register.index') }}">Datakan Diri Anda</a>!</p>
          <a class="btn btn-dark btn-xl js-scroll-trigger" href="#menu">MENU</a>
        </div>
      </div>
    </div>
  </section>

  <!-- menu -->
  <section class="content-section bg-primary text-white text-center" id="menu">
    <div class="container">
      <div class="content-section-heading">
        <h2 class="text-secondary mb-0">MENU</h2>
        <br>
        <br>
        <br>
        <br>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
        <span class="service-icon rounded-circle mx-auto mb-3">
          <a href="#page-top"><i class="icon-screen-smartphone"></i></a>
          </span>
          <h4>
            <strong>Home</strong>
          </h4>
        </div>
        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <a href="https://news.google.com/covid19/map?hl=id&mid=/m/02j71&gl=ID&ceid=ID:id"><i class="icon-pencil"></i></a>
          </span>
          <h4>
            <strong>News</strong>
          </h4>
        </div>
        <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
          <a href="{{ route('register.index') }}"><i class="icon-note"></i></a>
          </span>
          <h4>
            <strong>Register</strong>
          </h4>
        </div>
        <div class="col-lg-3 col-md-6">
          <span class="service-icon rounded-circle mx-auto mb-3">
          <a href="{{ route('login.index') }}"><i class="icon-login"></i></a>
          </span>
          <h4>
            <strong>Login</strong>
          </h4>
        </div>
      </div>
    </div>
  </section>

  <!-- Map -->
  <section>
      <br>
      <br>
      <center><h1>MAPS</h1></center>
      <br>
      
</section>
  <div id="contact" class="map">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5595.028768736776!2d109.26271434152711!3d-7.433928332843565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655e9b4dff9fc5%3A0x33a58454c5dd0e54!2sPerumahan%20Berkoh%20Indah!5e0!3m2!1sid!2sid!4v1594746421365!5m2!1sid!2sid" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    <br />
    <small>
      <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5595.028768736776!2d109.26271434152711!3d-7.433928332843565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655e9b4dff9fc5%3A0x33a58454c5dd0e54!2sPerumahan%20Berkoh%20Indah!5e0!3m2!1sid!2sid!4v1594746421365!5m2!1sid!2sid" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></a>
    </small>
  </div>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <ul class="list-inline mb-5">
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#!">
            <i class="icon-social-facebook"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#!">
            <i class="icon-social-twitter"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white" href="#!">
            <i class="icon-social-github"></i>
          </a>
        </li>
      </ul>
    </div>
  </footer>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/stylish-portfolio.min.js"></script>

</body>

</html>
