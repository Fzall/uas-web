<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--===============================================================================================-->
    <link rel="stylesheet" href="../register/fonts/material-icon/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" href="../register/css/style.css">
<!--===============================================================================================-->

    <title>Register</title>
</head>
    <!-- Font Icon -->

<body >

<div class="main">

<div class="container">
    <form action="{{ route('register.store') }}" method="POST" enctype="multipart/form-data" class="appointment-form" id="appointment-form">
    @csrf
        <h1 class="fontss">REGISTER</h1>
        <br>
        
        <div class="form-group-1">
            <label for="nim">NIM</label>
                    <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim"
                        name="nim" value="{{ old('nim') }}">
                    @error('nim')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="nama">Nama Lengkap</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
                        name="nama" value="{{ old('nama') }}">
                    @error('nama')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="jurusan">Jurusan</label>
                        <select class="form-control" name="jurusan" id="jurusan">
                            <option value="Teknik Informatika"
                                {{ old('jurusan')=='Teknik Informatika' ? 'selected': '' }}>
                                Teknik Informatika
                            </option>
                            <option value="Sistem Informasi"
                                {{ old('jurusan')=='Sistem Informasi' ? 'selected': '' }}>
                                Sistem Informasi
                            </option>
                            <option value="Ilmu Komputer" {{ old('jurusan')=='Ilmu Komputer' ? 'selected': '' }}>
                                Ilmu Komputer
                            </option>
                            <option value="Teknik Komputer"
                                {{ old('jurusan')=='Teknik Komputer' ? 'selected': '' }}>
                                Teknik Komputer
                            </option>
                            <option value="Teknik Telekomunikasi"
                                {{ old('jurusan')=='Teknik Telekomunikasi' ? 'selected': '' }}>
                                Teknik Telekomunikasi
                            </option>
                        </select>
                        @error('jurusan')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror

                    <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                            <option value="Laki-Laki"
                                {{ old('jenis_kelamin')=='Laki-Laki L' ? 'selected': '' }}>
                                Laki-Laki
                            </option>
                            <option value="Perempuan"
                                {{ old('jenis_kelamin')=='Perempuan P' ? 'selected': '' }}>
                                Perempuan
                            </option>
                        </select>
                        @error('jenis_kelamin')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    
                    <div class="from-group">
                    <label for="alamat">Alamat</label>
                        <br>
                        <textarea class="form-control" id="alamat" rows="5" 
                        name="alamat">{{ old('alamat') }}</textarea>
                    </div>
                    <br>
                    <label for="email">Email</label>
                    <input type="text" class="form-control @error('email') is-invalid @enderror" id="email"
                        name="email" value="{{ old('email') }}">
                    @error('email')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="username">Username</label>
                    <input type="text" class="form-control @error('username') is-invalid @enderror" id="username"
                        name="username" value="{{ old('username') }}">
                    @error('username')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="password">Password</label>
                    <input type="text" class="form-control @error('password') is-invalid @enderror" id="password"
                        name="password" value="{{ old('password') }}">
                    @error('password')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                    
                    <label for="image">Gambar Profile</label>
                    <input type="file" class="form-control-file" id="image" name="image">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

           
        </div>
            <div class="form-submit">
                    <input type="submit" name="submit" id="submit" class="submit" value="Register" />
            </div>
            <div class="form-submit">
                        <a href="{{ url('') }}/login" class="submit"><h4>Login</h4></a>
            </div>
        </div>

        
        
    </form>
</div>

</div>

<!-- JS -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/main.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>