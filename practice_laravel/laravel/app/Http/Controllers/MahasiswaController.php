<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function create(){
        return view('userlte.create');
    }
    
    public function show($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('userlte.show',['users' => $result]);
    }
    
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:students',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'email' => 'required' ,
            'image' => 'file|image|max:10240',
        ]);
        
        $mahasiswa = new User();
        $mahasiswa->nim = $validateData['nim'];
        $mahasiswa->name = $validateData['nama'];
        $mahasiswa->email = $validateData['email'];
        $mahasiswa->username = $validateData['username'];
        $mahasiswa->password = $validateData['password'];

        $mahasiswa->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('userlte.index');
    }

    public function index()
    {
        $mahasiswas = User::all();
        return view('userlte.index',['user' => $mahasiswas]);
    }
    
    public function edit($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('userlte.edit',['student' => $result]);
    }

    public function update(Request $request, Student $student)
    {
        $validateData = $request->validate([
        'nim' => 'required|size:8,unique:students',
        'nama' => 'required|min:3|max:50',
        'jenis_kelamin' => 'required',
        'jurusan' => 'required',
        'alamat' => '',
        'image' => 'file|image|max:10240',
        ]);
        
        $student->nim = $validateData['nim'];
        $student->name = $validateData['nama'];
        $student->jenis_kelamin = $validateData['jenis_kelamin'];
        $student->jurusan = $validateData['jurusan'];
        $student->alamat = $validateData['alamat'];
        $mahasiswa->email = $validateData['email'];
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($student->image);
            $path = $request->image->move('assets/images',$namaFile);
            $student->image = $path;
        }
        $student->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('userlte.show',['student' => $student->id]);
    }

    public function destroy(Request $request, Student $student)
    {
        File::delete($student->image);
        $student->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('student.index');
    }

}
