<?php

namespace App\Http\Controllers;
use App\Student;
use Illuminate\Http\Request;
use File;

class StudentController extends Controller
{
    public function create(){
        return view('student.create');
    }
    
    public function show($student_id)
    {
        $result = Student::findOrFail($student_id);
        return view('student.show',['student' => $result]);
    }
    
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:10240'
        ]);
        
        $mahasiswa = new Student();
        $mahasiswa->nim = $validateData['nim'];
        $mahasiswa->name = $validateData['nama'];
        $mahasiswa->email = $validateData['email'];
        $mahasiswa->username = $validateData['username'];
        $mahasiswa->password = $validateData['password'];
        $mahasiswa->jenis_kelamin = $validateData['jenis_kelamin'];
        $mahasiswa->jurusan = $validateData['jurusan'];
        $mahasiswa->alamat = $validateData['alamat'];
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image->move('assets/images',$namaFile);
            $mahasiswa->image = $path;
        }
        $mahasiswa->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('student.index');
    }

    public function index()
    {
        $mahasiswas = Student::all();
        return view('student.index',['students' => $mahasiswas]);
    }
    
    public function edit($student_id)
    {
        $result = Student::findOrFail($student_id);
        return view('student.edit',['student' => $result]);
    }

    public function update(Request $request, Student $student)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:10240'
        ]);
        
        $mahasiswa->nim = $validateData['nim'];
        $mahasiswa->name = $validateData['nama'];
        $mahasiswa->email = $validateData['email'];
        $mahasiswa->username = $validateData['username'];
        $mahasiswa->password = $validateData['password'];
        $mahasiswa->jenis_kelamin = $validateData['jenis_kelamin'];
        $mahasiswa->jurusan = $validateData['jurusan'];
        $mahasiswa->alamat = $validateData['alamat'];
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($student->image);
            $path = $request->image->move('assets/images',$namaFile);
            $student->image = $path;
        }
        $student->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('student.show',['student' => $student->id]);
    }

    public function destroy(Request $request, Student $student)
    {
        File::delete($student->image);
        $student->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('student.index');
    }

}