<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use File;
use App\Job;
use Illuminate\Support\Facades\DB;

class AdminLTEStudentController extends Controller
{
    public function create(){
        return view('adminlte.student.create');
    }
    
    
    public function show($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('adminlte.student.show',['student' => $result]);
    }
    public function showjob($student_id)
    {
        $result = Job::findOrFail($student_id);
        return view('adminlte.student.showjob',['student' => $result]);
    }
    
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:10240'
            ]);
            
            $mahasiswa = new User();
            $mahasiswa->nim = $validateData['nim'];
            $mahasiswa->name = $validateData['nama'];
            $mahasiswa->email = $validateData['email'];
            $mahasiswa->username = $validateData['username'];
            $mahasiswa->password = $validateData['password'];
            $mahasiswa->jenis_kelamin = $validateData['jenis_kelamin'];
            $mahasiswa->jurusan = $validateData['jurusan'];
            $mahasiswa->alamat = $validateData['alamat'];
            
    
            if($request->hasFile('image'))
            {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                $path = $request->image->move('assets/images',$namaFile);
                $mahasiswa->image = $path;
            }
    
            $mahasiswa->save();
            $request->session()->flash('pesan','Penambahan data berhasil');
            return redirect()->route('adminlte.student.index');
        }
        
        public function index()
        {
            $mahasiswas = User::all();
            return view('adminlte.student.index',['students' => $mahasiswas]);
        }
    


    
    public function dashboard()
    {
        $countStudent['jumlah_mahasiswa'] = DB::table('users')->count();
        $countJob['jumlah_pekerjaan'] = DB::table('jobs')->count();
        return view('adminlte.student.dashboard',['countStudent' => $countStudent], ['countJob' => $countJob]);
    }
    
    public function edit($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('adminlte.student.edit',['student' => $result]);
    }

    public function update(Request $request, User $student)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:1000'
            ]);
            
            $student->nim = $validateData['nim'];
            $student->name = $validateData['nama'];
            $student->email = $validateData['email'];
            $student->username = $validateData['username'];
            $student->password = $validateData['password'];
            $student->jenis_kelamin = $validateData['jenis_kelamin'];
            $student->jurusan = $validateData['jurusan'];
            $student->alamat = $validateData['alamat'];
            
    
            if($request->hasFile('image'))
            {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                File::delete($student->image);
                $path = $request->image->move('assets/images',$namaFile);
                $student->image = $path;
            }


        $student->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('adminlte.student.show',['student' => $student->id]);
        }
    

    public function destroy(Request $request, User $student)
    {
        File::delete($student->image);
        $student->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('adminlte.student.index');
    }
}